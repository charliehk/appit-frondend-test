import React, { useEffect, useState } from 'react';
import { Box } from '@material-ui/core';

function Card({index, colour, isDisable, isFlipped, handleCardStatus}) {

    const handleChange = () => {
        if (isFlipped | isDisable) {
            return;
        }
        handleCardStatus(colour, index)
        
      
    }
  return (
     <Box
        key={index}
        width='20%'
        height={100}
        bgcolor={isFlipped?colour:'black'}
        zIndex={isDisable? '-1':'1'}
        m={1}
        onClick={handleChange}
        >

    </Box>
  );
}

export default Card;
