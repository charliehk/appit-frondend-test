import React, { useEffect, useState } from 'react';
import { Box, Dialog, Button, FormControl, Input } from '@material-ui/core';
import Card from './Card'

function GameBody({ cards, score, storeData }) {

    const [openedCards, setOpenedCards] = useState([])
    const [pairedCards, setPairedCards] = useState([])
    const [isDialogOpen, setIsDialogOpen] = useState(false)
    const [playerScore, setPlayerScore] = useState(0)


    const handleCardStatus = (colour, index) => {
 
        if (openedCards.length === 0) {
            setOpenedCards([{ 'colour': colour, 'index': index }])
            return
        }
        if (openedCards.length === 1) {
            setOpenedCards(prev => [...prev, { 'colour': colour, 'index': index }])
                //if find the same colour card
                if (openedCards.find(card => card.colour === colour)) {
                    setPairedCards(prev => ([...prev, openedCards[0].index, index]))
                setTimeout(() => {;
                    setOpenedCards([])
                    score(playerScore + 5)
                    setPlayerScore(playerScore + 5)

                }, 1000);
                return
            } else {
                //if the colour is not match
                setTimeout(() => {
                    setOpenedCards([])
                    score(playerScore - 1)
                    setPlayerScore(playerScore - 1)

                }, 1000);
                return
            }
        } 
    }

    const isFlipped = (index) => {
        if (openedCards.find(card => card.index === index)) {
            return true
        }
        return false
    };

    const isDisable = (index) => {
        if (pairedCards.find(card => card === index)) {
            return true
        }
        return false
    };
 
    const handleClose = () => {
        setIsDialogOpen(false)
    }

    useEffect(() => {
       if(pairedCards.length === 16) {
           setIsDialogOpen(true)
           setPlayerScore(0)
       }
    }, [pairedCards])

    const handleSubmit = (event) => {
        event.preventDefault()
            
            storeData(event.target.name.value,playerScore)
            setIsDialogOpen(false)
    }
    return (
        <div >
        <Box
            mt={3}
            px={1}
            py={5}
            width='100%'
            maxHeight={800}
            display="flex"
            justifyContent='center'
            alignItems='center'
            flexWrap="wrap"
            bgcolor='gray'
        >
            
            {cards.map((colour, index) =>
                <Card
                    key={index}
                    index={index}
                    colour={colour}
                    isDisable={isDisable(index)}
                    isFlipped={isFlipped(index)}
                    handleCardStatus={handleCardStatus}
                />)}
        </Box>
        
        
        <Dialog  open={isDialogOpen} onClose={handleClose} >
            <Box 
            bgcolor='white' 
            p={5} 
            display='flex'
            flexDirection='column'
            justifyContent='center' >
                <form onSubmit={handleSubmit}>
                    <Input 
                    name='name' 
                    placeholder='Enter Your Name'
                    pattern="[A-Za-z]"
                    maxLength={15}
                    required
                    />
                    <Box name="score" my={2}>Your Final Score: {playerScore}</Box>
                    <Button type='submit' color="primary" variant="contained" >Submit</Button>
                </form>
            </Box>
            </Dialog>
 
      
</div>
    );
}

export default GameBody;
