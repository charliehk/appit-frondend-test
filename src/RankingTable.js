import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import  { useEffect, useState} from 'react';
import { Button } from '@material-ui/core';
import PropTypes from 'prop-types';
const useStyles = makeStyles({
  table: {
    minWidth: 305,
  },
});

function createData(rank, name, score) {
  return { rank, name, score };
}



function RankingTable({rankingData}) {
  const classes = useStyles();
  const [ranking, setRanking] = useState([])

  const reorderRanking =(...rankingData)=>{
     rankingData.sort((a,b) => {
    return a.score - b.score
  })
}

  useEffect (() => {  
  setRanking(prev => ([...prev, rankingData]))
  
    if (rankingData.length > 1) {
    const updatedRanking = reorderRanking(rankingData)
    setRanking(updatedRanking)
    } else {
      setRanking([rankingData])
    }

    
    
  },[rankingData])

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Rank</TableCell>
            <TableCell align="center">Name</TableCell>
            <TableCell align="center">Score</TableCell>
     
          </TableRow>
        </TableHead>
        <TableBody>
          {ranking.length > 0 && ranking.map((row, index) => (
            <TableRow key={row.name}>
             <TableCell component="th" scope="row">
                {index + 1}
              </TableCell>
              
              <TableCell align="center">{row.name}</TableCell>
              <TableCell align="center">{row.score}</TableCell>
            
            </TableRow>
          ))}
          {ranking.length < 0 && "No Player"}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

RankingTable.prototype = {
  rankingData: PropTypes.array.isRequired
}

export default RankingTable