import logo from './logo.png';
import './App.css';
import React, { useEffect, useState } from 'react';
import { Box, Button, Dialog } from '@material-ui/core';
import GameBody from './GameBody'
import RankingTable from './RankingTable'
function App() {

  const colourArray = ['red', 'blue', 'green', 'purple', 'orange', 'yellow', 'chartreuse', 'pink']
  const [cards, setCards] = useState([])
  const [playerScore, setPlayerScore] = useState(0)
  const [showTable, setShowTable] = useState(false)
  const [rankingData, setRankingData] = useState({})
  const randomOrders = (array) => {
    const cardsColourArray = array.concat(array)
    cardsColourArray.sort(() => Math.random() - 0.5);
    return cardsColourArray
  }


  useEffect(() => {
    const cardArray = randomOrders(colourArray)
    setCards(cardArray)
  }, [])

  const handleClose = () => {
    setShowTable(false)
  }
  const handleScore = (score) => {
    setPlayerScore(score)
  }

  const handleNewScore = (name, score) => {
    setRankingData({'name':name, 'score':score})
    
  }

  return (
    <div className="App">
      <Box display='flex' alignItems='center' justifyContent='space-between'>
        <img id='logo' src={logo} alt='logo' />
        <div>Score: {playerScore}</div>
        <Button 
        color="primary" 
        variant="contained" 
        onClick={() => setShowTable(true)}
        >Score Table
        </Button>
      </Box>
        <GameBody cards={cards} score={handleScore} storeData={handleNewScore}/>
       <Dialog open={showTable} >
         <RankingTable rankingData={rankingData} handleClose={handleClose}/>
         <Button onClick={() => setShowTable(false)}>Close</Button>
       </Dialog> 
    </div>
  );
}

export default App;
