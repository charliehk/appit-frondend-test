### `Start the game`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
As it is designed for mobile, it is suggested to click "Inspect" and change to mobile view.


### `Play the game`
Flipped the card to find the same colour.
If you match the color, you will get 5 points, if you don't, 1 point will be deducted.
After you match all the cards, type your name and it will appear on the score table.

